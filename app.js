var nuriModule = angular.module('AnyGap',[]);
nuriModule.factory('AngelloHelper', function(){});
nuriModule.service('AngelloService', function(){
  var service = this;
  var stories = [
    {
      title: '첫번째 스토리',
      description: '첫번째 사용자 스토리',
      criteria: '요구사항 정리중 ... ',
      status: '해야할 일',
      type: '기능',
      reporter: '웹지니',
      assignee: '웹지니'
    },
    {
      title: '두번째 스토리',
      description: '두번째 사용자 스토리',
      criteria: '요구사항 정리중 ... ',
      status: '백로그',
      type: '기능',
      reporter: '웹지니',
      assignee: '웹지니'
    },
    {
      title: '세번째 스토리',
      description: '세번째 사용자 스토리',
      criteria: '요구사항 정리중 ... ',
      status: '코드 리뷰',
      type: '개선',
      reporter: '웹지니',
      assignee: '웹지니'
    }
  ];

  service.getStories = function(){
    return stories;
  };
});
nuriModule.directive('story', function(){
  return {
    scope: true,
    replace: true,
    template: '<div><h4>{{story.title}}</h4><p>{{story.description}}</p></div>'
  };
});

nuriModule.controller('MainCtrl', function(AngelloService, $scope){
	var vm = this;
  console.log('$scope :: ', $scope);
  
  // vm.stories = AngelloService.getStories();
  vm.stories = [
    {
      title: '첫번째 스토리',
      description: '첫번째 사용자 스토리',
      criteria: '요구사항 정리중 ... ',
      status: '해야할 일',
      type: '기능',
      reporter: '웹지니',
      assignee: '웹지니'
    },
    {
      title: '두번째 스토리',
      description: '두번째 사용자 스토리',
      criteria: '요구사항 정리중 ... ',
      status: '백로그',
      type: '기능',
      reporter: '웹지니',
      assignee: '웹지니'
    },
    {
      title: '세번째 스토리',
      description: '세번째 사용자 스토리',
      criteria: '요구사항 정리중 ... ',
      status: '코드 리뷰',
      type: '개선',
      reporter: '웹지니',
      assignee: '웹지니'
    }
  ];

  vm.createStory = function(){
    vm.stories.push({
      title: '새 사용자 스토리',
      description: '데스....페라도',
      criteria: '요구사항 정리중 ... ',
      status: '백로그',
      type: '기능',
      reporter: '--',
      assignee: '--'
    });
  };

});
